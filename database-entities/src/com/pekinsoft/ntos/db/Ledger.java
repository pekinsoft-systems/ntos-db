/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Ledger.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.TransactionType;
import java.io.Serializable;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The {@code Ledger} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Ledger.findAll",
            query = "SELECT l FROM Ledger l"),
    @NamedQuery(name = "Ledger.findById",
            query = "SELECT l FROM Ledger l WHERE l.id = :id"),
    @NamedQuery(name = "Ledger.findByTxDate",
            query = "SELECT l FROM Ledger l WHERE l.txDate = :txDate"),
    @NamedQuery(name = "Ledger.findByTxType",
            query = "SELECT l FROM Ledger l WHERE l.txType = :txType"),
    @NamedQuery(name = "Ledger.findByReference",
            query = "SELECT l FROM Ledger l WHERE l.reference = :reference"),
    @NamedQuery(name = "Ledger.findByDescription",
            query = "SELECT l FROM Ledger l WHERE l.description = :description"),
    @NamedQuery(name = "Ledger.findByAmount",
            query = "SELECT l FROM Ledger l WHERE l.amount = :amount"),
    @NamedQuery(name = "Ledger.findByBalanced",
            query = "SELECT l FROM Ledger l WHERE l.balanced = :balanced")})
public class Ledger implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "TX_DATE", nullable = false, length = 30)
    private LocalDate txDate;
    @Basic(optional = false)
    @Column(name = "TX_TYPE", nullable = false, length = 10)
    private TransactionType txType;
    @Column(length = 20)
    private String reference;
    @Column(length = 150)
    private String description;
    @Basic(optional = false)
    @Column(nullable = false)
    private double amount;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean balanced;
    @JoinColumn(name = "ACCOUNT_NUMBER", referencedColumnName = "ACCOUNT_NUMBER", nullable = false)
    @ManyToOne(optional = false)
    private Account accountNumber;

    public Ledger() {

    }

    public Ledger(Long id) {
        this.id = id;
    }

    public Ledger(Long id, LocalDate txDate, TransactionType txType,
            double amount, Boolean balanced) {
        this.id = id;
        this.txDate = txDate;
        this.txType = txType;
        this.amount = amount;
        this.balanced = balanced;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getTxDate() {
        return txDate;
    }

    public void setTxDate(LocalDate txDate) {
        this.txDate = txDate;
    }

    public TransactionType getTxType() {
        return txType;
    }

    public void setTxType(TransactionType txType) {
        this.txType = txType;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Boolean getBalanced() {
        return balanced;
    }

    public void setBalanced(Boolean balanced) {
        this.balanced = balanced;
    }

    public Account getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(Account accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Ledger)) {
            return false;
        }
        Ledger other = (Ledger) object;
        return !((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        String txDate = DateTimeFormatter.ISO_LOCAL_DATE.format(this.txDate);
        String amount = NumberFormat.getCurrencyInstance().format(this.amount);
        String txCode = txType.name();
        String ref = (reference != null) ? " [ REF: " + reference + " ]" : "";
        return txDate + " for " + amount + " by " + txCode + ref;
    }

}
