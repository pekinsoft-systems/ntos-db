/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   ServiceRecord.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The {@code ServiceRecord} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "SERVICE_RECORDS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "ServiceRecord.findAll",
            query = "SELECT s FROM ServiceRecord s"),
    @NamedQuery(name = "ServiceRecord.findById",
            query = "SELECT s FROM ServiceRecord s WHERE s.id = :id"),
    @NamedQuery(name = "ServiceRecord.findByServiceDate",
            query = "SELECT s FROM ServiceRecord s WHERE s.serviceDate = :serviceDate"),
    @NamedQuery(name = "ServiceRecord.findByLastServiceDate",
            query = "SELECT s FROM ServiceRecord s WHERE s.lastServiceDate = :lastServiceDate"),
    @NamedQuery(name = "ServiceRecord.findByOdometer",
            query = "SELECT s FROM ServiceRecord s WHERE s.odometer = :odometer"),
    @NamedQuery(name = "ServiceRecord.findByDistanceSinceLastService",
            query = "SELECT s FROM ServiceRecord s WHERE s.distanceSinceLastService = :distanceSinceLastService"),
    @NamedQuery(name = "ServiceRecord.findByEstimatedCost",
            query = "SELECT s FROM ServiceRecord s WHERE s.estimatedCost = :estimatedCost"),
    @NamedQuery(name = "ServiceRecord.findByCompletionDate",
            query = "SELECT s FROM ServiceRecord s WHERE s.completionDate = :completionDate"),
    @NamedQuery(name = "ServiceRecord.findByActualCost",
            query = "SELECT s FROM ServiceRecord s WHERE s.actualCost = :actualCost")})
public class ServiceRecord implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "SERVICE_DATE", nullable = false, length = 30)
    private LocalDate serviceDate;
    @Basic(optional = false)
    @Column(name = "LAST_SERVICE_DATE", nullable = false, length = 30)
    private LocalDate lastServiceDate;
    @Basic(optional = false)
    @Column(nullable = false)
    private int odometer;
    @Basic(optional = false)
    @Column(name = "DISTANCE_SINCE_LAST_SERVICE", nullable = false)
    private int distanceSinceLastService;
    @Basic(optional = false)
    @Lob
    @Column(name = "PROBLEM_DESCRIPTION", nullable = false, length = 32700)
    private String problemDescription;
    @Basic(optional = false)
    @Column(name = "ESTIMATED_COST", nullable = false)
    private long estimatedCost;
    @Lob
    @Column(name = "PARTS_REPLACED", length = 32700)
    private String partsReplaced;
    @Basic(optional = false)
    @Lob
    @Column(name = "WORK_PERFORMED", nullable = false, length = 32700)
    private String workPerformed;
    @Basic(optional = false)
    @Column(name = "COMPLETION_DATE", nullable = false, length = 30)
    private LocalDate completionDate;
    @Basic(optional = false)
    @Column(name = "ACTUAL_COST", nullable = false)
    private long actualCost;
    @JoinColumn(name = "UNIT_NUMBER", referencedColumnName = "UNIT_NUMBER", nullable = false)
    @ManyToOne(optional = false)
    private Asset unitNumber;

    public ServiceRecord() {

    }

    public ServiceRecord(Long id) {
        this.id = id;
    }

    public ServiceRecord(Long id, LocalDate serviceDate,
            LocalDate lastServiceDate, int odometer, int distanceSinceLastService,
            String problemDescription, long estimatedCost, String workPerformed,
            LocalDate completionDate, long actualCost) {
        this.id = id;
        this.serviceDate = serviceDate;
        this.lastServiceDate = lastServiceDate;
        this.odometer = odometer;
        this.distanceSinceLastService = distanceSinceLastService;
        this.problemDescription = problemDescription;
        this.estimatedCost = estimatedCost;
        this.workPerformed = workPerformed;
        this.completionDate = completionDate;
        this.actualCost = actualCost;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(LocalDate serviceDate) {
        this.serviceDate = serviceDate;
    }

    public LocalDate getLastServiceDate() {
        return lastServiceDate;
    }

    public void setLastServiceDate(LocalDate lastServiceDate) {
        this.lastServiceDate = lastServiceDate;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public int getDistanceSinceLastService() {
        return distanceSinceLastService;
    }

    public void setDistanceSinceLastService(int distanceSinceLastService) {
        this.distanceSinceLastService = distanceSinceLastService;
    }

    public String getProblemDescription() {
        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    public long getEstimatedCost() {
        return estimatedCost;
    }

    public void setEstimatedCost(long estimatedCost) {
        this.estimatedCost = estimatedCost;
    }

    public String getPartsReplaced() {
        return partsReplaced;
    }

    public void setPartsReplaced(String partsReplaced) {
        this.partsReplaced = partsReplaced;
    }

    public String getWorkPerformed() {
        return workPerformed;
    }

    public void setWorkPerformed(String workPerformed) {
        this.workPerformed = workPerformed;
    }

    public LocalDate getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(LocalDate completionDate) {
        this.completionDate = completionDate;
    }

    public long getActualCost() {
        return actualCost;
    }

    public void setActualCost(long actualCost) {
        this.actualCost = actualCost;
    }

    public Asset getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(Asset unitNumber) {
        this.unitNumber = unitNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ServiceRecord)) {
            return false;
        }
        ServiceRecord other = (ServiceRecord) object;
        return !((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        String makeModel = unitNumber.getMake() + "/" + unitNumber.getModel();
        String svcDate = DateTimeFormatter.ISO_LOCAL_DATE.format(serviceDate);
        String cmpDate;
        if (completionDate != null) {
            cmpDate = DateTimeFormatter.ISO_LOCAL_DATE.format(completionDate);
        } else {
            cmpDate = "[Not Done]";
        }
        return "Service on " + makeModel + " begun on " + svcDate
                + " completed on " + cmpDate;
    }

}
