/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Customer.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The {@code Customer} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "CUSTOMERS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Customer.findAll", query = "SELECT c FROM Customer c"),
    @NamedQuery(name = "Customer.findById", query = "SELECT c FROM Customer c WHERE c.id = :id"),
    @NamedQuery(name = "Customer.findByCompany", query = "SELECT c FROM Customer c WHERE c.company = :company"),
    @NamedQuery(name = "Customer.findByStreet", query = "SELECT c FROM Customer c WHERE c.street = :street"),
    @NamedQuery(name = "Customer.findBySuite", query = "SELECT c FROM Customer c WHERE c.suite = :suite"),
    @NamedQuery(name = "Customer.findByCity", query = "SELECT c FROM Customer c WHERE c.city = :city"),
    @NamedQuery(name = "Customer.findByStateOrProvince", query = "SELECT c FROM Customer c WHERE c.stateOrProvince = :stateOrProvince"),
    @NamedQuery(name = "Customer.findByPostalCode", query = "SELECT c FROM Customer c WHERE c.postalCode = :postalCode"),
    @NamedQuery(name = "Customer.findByContact", query = "SELECT c FROM Customer c WHERE c.contact = :contact"),
    @NamedQuery(name = "Customer.findByPhone", query = "SELECT c FROM Customer c WHERE c.phone = :phone"),
    @NamedQuery(name = "Customer.findByFax", query = "SELECT c FROM Customer c WHERE c.fax = :fax"),
    @NamedQuery(name = "Customer.findByEmail", query = "SELECT c FROM Customer c WHERE c.email = :email"),
    @NamedQuery(name = "Customer.findBySince", query = "SELECT c FROM Customer c WHERE c.since = :since"),
    @NamedQuery(name = "Customer.findBySalesTax", query = "SELECT c FROM Customer c WHERE c.salesTax = :salesTax"),
    @NamedQuery(name = "Customer.findByPaymentTerms", query = "SELECT c FROM Customer c WHERE c.paymentTerms = :paymentTerms"),
    @NamedQuery(name = "Customer.findByInactive", query = "SELECT c FROM Customer c WHERE c.inactive = :inactive"),
    @NamedQuery(name = "Customer.findByCustomerType", query = "SELECT c FROM Customer c WHERE c.customerType = :customerType")})
public class Customer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 10)
    private String id;
    @Basic(optional = false)
    @Column(nullable = false, length = 50)
    private String company;
    @Basic(optional = false)
    @Column(nullable = false, length = 40)
    private String street;
    @Column(length = 20)
    private String suite;
    @Basic(optional = false)
    @Column(nullable = false, length = 40)
    private String city;
    @Basic(optional = false)
    @Column(name = "STATE_OR_PROVINCE", nullable = false, length = 2)
    private String stateOrProvince;
    @Basic(optional = false)
    @Column(name = "POSTAL_CODE", nullable = false, length = 15)
    private String postalCode;
    @Column(length = 40)
    private String contact;
    @Column(length = 14)
    private String phone;
    @Column(length = 14)
    private String fax;
    @Column(length = 40)
    private String email;
    @Basic(optional = false)
    @Column(nullable = false, length = 30)
    private String since;
    @Basic(optional = false)
    @Column(name = "SALES_TAX", nullable = false, length = 25)
    private String salesTax;
    @Basic(optional = false)
    @Column(name = "PAYMENT_TERMS", nullable = false, length = 50)
    private String paymentTerms;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean inactive;
    @Basic(optional = false)
    @Column(name = "CUSTOMER_TYPE", nullable = false, length = 10)
    private String customerType;
    @Lob
    @Column(length = 32700)
    private String notes;
    @JoinColumn(name = "DEFAULT_ACCOUNT", referencedColumnName = "ACCOUNT_NUMBER", nullable = false)
    @ManyToOne(optional = false)
    private Account defaultAccount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerId")
    private List<Stop> stopList;

    public Customer() {

    }

    public Customer(String id) {
        this.id = id;
    }

    public Customer(String id, String company, String street, String city, String stateOrProvince, String postalCode, String since, String salesTax, String paymentTerms, Boolean inactive, String customerType) {
        this.id = id;
        this.company = company;
        this.street = street;
        this.city = city;
        this.stateOrProvince = stateOrProvince;
        this.postalCode = postalCode;
        this.since = since;
        this.salesTax = salesTax;
        this.paymentTerms = paymentTerms;
        this.inactive = inactive;
        this.customerType = customerType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getSalesTax() {
        return salesTax;
    }

    public void setSalesTax(String salesTax) {
        this.salesTax = salesTax;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public void setPaymentTerms(String paymentTerms) {
        this.paymentTerms = paymentTerms;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Account getDefaultAccount() {
        return defaultAccount;
    }

    public void setDefaultAccount(Account defaultAccount) {
        this.defaultAccount = defaultAccount;
    }

    public List<Stop> getStopList() {
        return stopList;
    }

    public void setStopList(List<Stop> stopList) {
        this.stopList = stopList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pekinsoft.ntos.db.Customer[ id=" + id + " ]";
    }

}
