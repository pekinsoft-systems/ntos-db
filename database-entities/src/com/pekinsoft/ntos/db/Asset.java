/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Asset.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.AssetStatus;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The {@code Asset} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "ASSETS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Asset.findAll",
            query = "SELECT a FROM Asset a"),
    @NamedQuery(name = "Asset.findByUnitNumber",
            query = "SELECT a FROM Asset a WHERE a.unitNumber = :unitNumber"),
    @NamedQuery(name = "Asset.findByCategory",
            query = "SELECT a FROM Asset a WHERE a.category = :category"),
    @NamedQuery(name = "Asset.findByMake",
            query = "SELECT a FROM Asset a WHERE a.make = :make"),
    @NamedQuery(name = "Asset.findByModel",
            query = "SELECT a FROM Asset a WHERE a.model = :model"),
    @NamedQuery(name = "Asset.findByModelYear",
            query = "SELECT a FROM Asset a WHERE a.modelYear = :modelYear"),
    @NamedQuery(name = "Asset.findBySerialVin",
            query = "SELECT a FROM Asset a WHERE a.serialVin = :serialVin"),
    @NamedQuery(name = "Asset.findByDateAcquired",
            query = "SELECT a FROM Asset a WHERE a.dateAcquired = :dateAcquired"),
    @NamedQuery(name = "Asset.findByPurchasePrice",
            query = "SELECT a FROM Asset a WHERE a.purchasePrice = :purchasePrice"),
    @NamedQuery(name = "Asset.findByDepreciationMethod",
            query = "SELECT a FROM Asset a WHERE a.depreciationMethod = :depreciationMethod"),
    @NamedQuery(name = "Asset.findByDepreciableLife",
            query = "SELECT a FROM Asset a WHERE a.depreciableLife = :depreciableLife"),
    @NamedQuery(name = "Asset.findBySalvageValue",
            query = "SELECT a FROM Asset a WHERE a.salvageValue = :salvageValue"),
    @NamedQuery(name = "Asset.findByCurrentValue",
            query = "SELECT a FROM Asset a WHERE a.currentValue = :currentValue"),
    @NamedQuery(name = "Asset.findByDateSold",
            query = "SELECT a FROM Asset a WHERE a.dateSold = :dateSold"),
    @NamedQuery(name = "Asset.findBySalesPrice",
            query = "SELECT a FROM Asset a WHERE a.salesPrice = :salesPrice"),
    @NamedQuery(name = "Asset.findByNextScheduledMaintenance",
            query = "SELECT a FROM Asset a WHERE a.nextScheduledMaintenance = :nextScheduledMaintenance"),
    @NamedQuery(name = "Asset.findByStatus",
            query = "SELECT a FROM Asset a WHERE a.status = :status")})
public class Asset implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final DateTimeFormatter FORMATTER
            = DateTimeFormatter.ISO_LOCAL_DATE;

    @Id
    @Basic(optional = false)
    @Column(name = "UNIT_NUMBER", nullable = false, length = 20)
    private String unitNumber;
    @Basic(optional = false)
    @Column(nullable = false, length = 30)
    private String category;
    @Basic(optional = false)
    @Column(nullable = false, length = 25)
    private String make;
    @Basic(optional = false)
    @Column(nullable = false, length = 25)
    private String model;
    @Basic(optional = false)
    @Column(name = "MODEL_YEAR", nullable = false)
    private int modelYear;
    @Basic(optional = false)
    @Column(name = "SERIAL_VIN", nullable = false, length = 25)
    private String serialVin;
    @Basic(optional = false)
    @Column(name = "DATE_ACQUIRED", nullable = false, length = 30)
    private LocalDate dateAcquired;
    @Basic(optional = false)
    @Column(name = "PURCHASE_PRICE", nullable = false)
    private long purchasePrice;
    @Basic(optional = false)
    @Column(name = "DEPRECIATION_METHOD", nullable = false, length = 25)
    private String depreciationMethod;
    @Basic(optional = false)
    @Column(name = "DEPRECIABLE_LIFE", nullable = false)
    private int depreciableLife;
    @Basic(optional = false)
    @Column(name = "SALVAGE_VALUE", nullable = false)
    private long salvageValue;
    @Basic(optional = false)
    @Column(name = "CURRENT_VALUE", nullable = false)
    private long currentValue;
    @Column(name = "DATE_SOLD", length = 30)
    private LocalDate dateSold;
    @Column(name = "SALES_PRICE")
    private Long salesPrice;
    @Basic(optional = false)
    @Column(name = "NEXT_SCHEDULED_MAINTENANCE", nullable = false, length = 30)
    private LocalDate nextScheduledMaintenance;
    @Basic(optional = false)
    @Column(nullable = false, length = 25)
    private AssetStatus status;
    @Lob
    @Column(length = 32700)
    private String description;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "unitNumber")
    private List<ServiceRecord> serviceRecordList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "assignedUnit")
    private List<Load> loadList;

    public Asset() {

    }

    public Asset(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public Asset(String unitNumber, String category, String make, String model,
            int modelYear, String serialVin, LocalDate dateAcquired,
            long purchasePrice, String depreciationMethod, int depreciableLife,
            long salvageValue, long currentValue,
            LocalDate nextScheduledMaintenance, AssetStatus status) {
        this.unitNumber = unitNumber;
        this.category = category;
        this.make = make;
        this.model = model;
        this.modelYear = modelYear;
        this.serialVin = serialVin;
        this.dateAcquired = dateAcquired;
        this.purchasePrice = purchasePrice;
        this.depreciationMethod = depreciationMethod;
        this.depreciableLife = depreciableLife;
        this.salvageValue = salvageValue;
        this.currentValue = currentValue;
        this.nextScheduledMaintenance = nextScheduledMaintenance;
        this.status = status;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getModelYear() {
        return modelYear;
    }

    public void setModelYear(int modelYear) {
        this.modelYear = modelYear;
    }

    public String getSerialVin() {
        return serialVin;
    }

    public void setSerialVin(String serialVin) {
        this.serialVin = serialVin;
    }

    public LocalDate getDateAcquired() {
        return dateAcquired;
    }

    public void setDateAcquired(LocalDate dateAcquired) {
        this.dateAcquired = dateAcquired;
    }

    public long getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(long purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getDepreciationMethod() {
        return depreciationMethod;
    }

    public void setDepreciationMethod(String depreciationMethod) {
        this.depreciationMethod = depreciationMethod;
    }

    public int getDepreciableLife() {
        return depreciableLife;
    }

    public void setDepreciableLife(int depreciableLife) {
        this.depreciableLife = depreciableLife;
    }

    public long getSalvageValue() {
        return salvageValue;
    }

    public void setSalvageValue(long salvageValue) {
        this.salvageValue = salvageValue;
    }

    public long getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(long currentValue) {
        this.currentValue = currentValue;
    }

    public LocalDate getDateSold() {
        return dateSold;
    }

    public void setDateSold(LocalDate dateSold) {
        this.dateSold = dateSold;
    }

    public Long getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(Long salesPrice) {
        this.salesPrice = salesPrice;
    }

    public LocalDate getNextScheduledMaintenance() {
        return nextScheduledMaintenance;
    }

    public void setNextScheduledMaintenance(LocalDate nextScheduledMaintenance) {
        this.nextScheduledMaintenance = nextScheduledMaintenance;
    }

    public AssetStatus getStatus() {
        return status;
    }

    public void setStatus(AssetStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ServiceRecord> getServiceRecordList() {
        return serviceRecordList;
    }

    public void setServiceRecordList(List<ServiceRecord> serviceRecordList) {
        this.serviceRecordList = serviceRecordList;
    }

    public List<Load> getLoadList() {
        return loadList;
    }

    public void setLoadList(List<Load> loadList) {
        this.loadList = loadList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (unitNumber != null ? unitNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Asset)) {
            return false;
        }
        Asset other = (Asset) object;
        return !((this.unitNumber == null && other.unitNumber != null)
                || (this.unitNumber != null && !this.unitNumber.equals(other.unitNumber)));
    }

    @Override
    public String toString() {
        return unitNumber + " [ Make/Model: " + make + "/" + model
                + " ] (Status: " + status.toString() + ")";
    }

}
