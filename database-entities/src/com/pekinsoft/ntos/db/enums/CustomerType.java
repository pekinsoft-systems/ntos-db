/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   CustomerType.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code CustomerType} enumeration provides standard constants to use for
 * defining the type of customer a database record represents. This enumeration
 * is used to prevent needing multiple tables in the database that store
 * virtually the same information for customers, vendors, brokers, and/or
 * agents. Instead, using this enumeration, there can be a field in the table
 * for storing the type of customer the record represents.
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum CustomerType {

    CUSTOMER("Customer"),
    VENDOR("Vendor"),
    BROKER("Broker"),
    AGENT("Agent");

    private final String type;

    private CustomerType(String type) {
        this.type = type;
    }

    /**
     * Retrieves the string value of this enumerated constant, i.e., if called
     * on the constant {@code CustomerType.VENDOR}, the returned value is
     * "Vendor".
     *
     * @return the string value representation of this {@code CustomerType}
     * constant
     */
    @Override
    public String toString() {
        return type;
    }

    /**
     * Creates an {@code CustomerType} constant of the given string value. This
     * method is mostly for use when a <em>Persistence Entity</em> is reading
     * the value from the database, in order to store the enumeration constant
     * in an entity field for the application.
     *
     * @param type the string value of the enumeration constant type
     * @return the {@code CustomerType} enumeration constant represented by the
     * string value supplied
     * @throws IllegalArgumentException if the specified {@code type} is not a
     * known {@code CustomerType} constant
     */
    public static final CustomerType of(String type) {
        switch (type.toLowerCase()) {
            case "customer":
                return CUSTOMER;
            case "vendor":
                return VENDOR;
            case "broker":
                return BROKER;
            case "agent":
                return AGENT;
            default:
                throw new IllegalArgumentException("Unknown customer type: " + type);
        }
    }

}
