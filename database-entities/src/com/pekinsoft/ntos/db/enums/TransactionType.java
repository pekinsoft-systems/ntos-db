/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   TransactionType.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code TransactionType} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum TransactionType {

    ATM("ATM"),
    CC("Credit Card"),
    CHK("Check"),
    DC("Debit Card"),
    EFT("Electronic Funds Transfer"),
    FC("Fuel Card"),
    ONLINE("Online Purchase"),
    PO("Purchase Order"),
    TELLER("Teller");

    private final String type;

    private TransactionType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static final TransactionType of(String type) {
        switch (type.toLowerCase()) {
            case "atm":
                return ATM;
            case "credit card":
                return CC;
            case "check":
                return CHK;
            case "debit card":
                return DC;
            case "electronic funds transfer":
                return EFT;
            case "fuel card":
                return FC;
            case "online purchase":
                return ONLINE;
            case "purchase order":
                return PO;
            case "teller":
                return TELLER;
            default:
                throw new IllegalArgumentException("Unknown transaction type: "
                        + type);
        }
    }

}
