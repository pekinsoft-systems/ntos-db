/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Commodity.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code Commodity} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum Commodity {

    AIRCRAFT("Aircraft: General"),
    AUTOS("Autos: General"),
    AUTOS_SPECIALTY("Autos: Specialty"),
    GENERAL_FREIGHT("General Freight: All Kinds"),
    DRY_BULK("Dry Bulk: All Kinds"),
    DRY_BULK_FOOD("Dry Bulk: Food Grade"),
    DRY_BULK_GRAIN("Dry Bulk: Grain"),
    DRY_BULK_PLASTIC("Dry Bulk: Plastic"),
    DRY_BULK_POWDER("Dry Bulk: Powder"),
    DRY_BULK_SAND("Dry Bulk: Sand/Gravel"),
    ENGINES("Engines: General"),
    ENGINES_AIRCRAFT("Engines: Aircraft"),
    ENGINES_AUTOMOTIVE("Engines: Automotive"),
    ENGINES_EQUIPMENT("Engines: Equipment"),
    EQUIPMENT("Equipment: All Kinds"),
    HEAVY_HAUL("Heavy Haul: All Kinds"),
    HEAVY_HAUL_MATERIALS("Heavy Haul: Materials"),
    HEAVY_HAUL_EQUIPMENT("Heavy Haul: Equipment"),
    LIQUID_BULK("Liquid Bulk: All Kinds"),
    LIQUID_BULK_AUTOMOTIVE("Liquid Bulk: Automotive"),
    LIQUID_BULK_FOOD("Liquid Bulk: Food Grade"),
    LIQUID_BULK_FUEL("Liquid Bulk: Fuel"),
    MACHINERY("Machinery: All Kinds"),
    METAL_ALUMUNIM("Metal: Aluminum"),
    METAL_IRON_OR_STEEL("Metal: Iron or Steel"),
    METAL_SHEETS_IN_COIL("Metal: Sheets in Coil"),
    PIPE("Pipe: All Kinds"),
    PIPE_CAST("Pipe: Concrete Castings"),
    PIPE_PLASTIC("Pipe: Plastic"),
    PIPE_STEEL_OR_IRON("Pipe: Steel or Iron");

    private final String commodity;

    private Commodity(String commodity) {
        this.commodity = commodity;
    }

    @Override
    public String toString() {
        return commodity;
    }

    public static final Commodity of(String commodity) {
        switch (commodity) {
            case "Aircraft: General":
                return AIRCRAFT;
            case "Autos: General":
                return AUTOS;
            case "Autos: Specialty":
                return AUTOS_SPECIALTY;
            case "General Freight: All Kinds":
                return GENERAL_FREIGHT;
            case "Dry Bulk: All Kinds":
                return DRY_BULK;
            case "Dry Bulk: Food Grade":
                return DRY_BULK_FOOD;
            case "Dry Bulk: Grain":
                return DRY_BULK_GRAIN;
            case "Dry Bulk: Plastic":
                return DRY_BULK_PLASTIC;
            case "Dry Bulk: Powder":
                return DRY_BULK_POWDER;
            case "Dry Bulk: Sand/Gravel":
                return DRY_BULK_SAND;
            case "Engines: General":
                return ENGINES;
            case "Engines: Aircraft":
                return ENGINES_AIRCRAFT;
            case "Engines: Automotive":
                return ENGINES_AUTOMOTIVE;
            case "Engines: Equipment":
                return ENGINES_EQUIPMENT;
            case "Equipment: All Kinds":
                return EQUIPMENT;
            case "Heavy Haul: All Kinds":
                return HEAVY_HAUL;
            case "Heavy Haul: Materials":
                return HEAVY_HAUL_MATERIALS;
            case "Heavy Haul: Equipment":
                return HEAVY_HAUL_EQUIPMENT;
            case "Liquid Bulk: All Kinds":
                return LIQUID_BULK;
            case "Liquid Bulk: Automotive":
                return LIQUID_BULK_AUTOMOTIVE;
            case "Liquid Bulk: Food Grade":
                return LIQUID_BULK_FOOD;
            case "Liquid Bulk: Fuel":
                return LIQUID_BULK_FUEL;
            case "Machinery: All Kinds":
                return MACHINERY;
            case "Metal: Aluminum":
                return METAL_ALUMUNIM;
            case "Metal: Iron or Steel":
                return METAL_IRON_OR_STEEL;
            case "Metal: Sheets in Coil":
                return METAL_SHEETS_IN_COIL;
            case "Pipe: All Kinds":
                return PIPE;
            case "Pipe: Concrete Castings":
                return PIPE_CAST;
            case "Pipe: Plastic":
                return PIPE_PLASTIC;
            case "Pipe: Steel or Iron":
                return PIPE_STEEL_OR_IRON;
            default:
                throw new IllegalArgumentException("Unknown commodity: "
                        + commodity);
        }
    }

}
