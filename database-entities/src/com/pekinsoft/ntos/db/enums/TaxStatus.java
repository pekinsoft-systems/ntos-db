/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   TaxStatus.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code TaxStatus} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum TaxStatus {

    SINGLE("Single"),
    MARRIED("Married"),
    MARRIED_HIGHER("Married, higher single rate"),
    MARRIED_SEPARATE("Married, filingly separately"),
    MARRIED_JOINTLY("Married, filing jointly"),
    HEAD_OF_HOUSEHOLD("Head of Household");

    private final String status;

    private TaxStatus(String status) {
        this.status = status;
    }

    /**
     * Retrieves the {@code TaxStatus} enumerated constant as a string
     * representation. For example,
     * {@code TaxStatus.MARRIED_SEPARATE.toString()} returns the string
     * "Married, filing separately".
     *
     * @return the string representation of the {@code TaxStatus} enumerated
     * constant
     */
    @Override
    public String toString() {
        return status;
    }

    /**
     * Retrieves the {@code TaxStatus} enumerated constant from the specified
     * string value. This method is typically only used by the {@link
     * com.pekinsoft.ntos.db.converters.TaxStatusConverter TaxStatusConverter}
     * for retrieving the data from the database.
     *
     * @param status the string value representation of the {@code TaxStatus}
     * enumerated constant desired
     * @return the {@code TaxStatus} enumerated constant denoted by the
     * specified string value
     * @throws IllegalArgumentException if the specified {@code status}
     * parameter is an unknown status
     */
    public static final TaxStatus of(String status) {
        switch (status.toLowerCase()) {
            case "single":
                return SINGLE;
            case "married":
                return MARRIED;
            case "married, higher single rate":
                return MARRIED_HIGHER;
            case "married, filing separately":
                return MARRIED_SEPARATE;
            case "married, filing jointly":
                return MARRIED_JOINTLY;
            case "head of household":
                return HEAD_OF_HOUSEHOLD;
            default:
                throw new IllegalArgumentException("Unknown tax status: " + status);
        }
    }

}
