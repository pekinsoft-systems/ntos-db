/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   AccountType.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code AccountType} enumerated constants specify the type of account that
 * is represented.
 * <p>
 * In accounting, there are five (5) common account types, and each one is
 * designated as either a <em>debit</em> or <em>credit</em> account. Unlike the
 * common use of those terms, they have a completely different meaning in
 * accounting.
 * <dl>
 * <dt><strong><em>Debit</em></strong>:</dt>
 * <dd>An account designated as a <em>debit</em> account has its balance
 * <strong>increased</strong> by debits to it, and <strong>decreased</strong> by
 * credits to it.<br>
 * <br>
 * As a real-world example, your checking account is carried on the books by you
 * as a <em>debit</em> account. This is because your checking account is an
 * <strong><em>asset</em></strong> to you because that money belongs to you.
 * Therefore, when the value increases, the account has been debited and when
 * the value decreases, it has been credit.<br>
 * <br>
 * This is confusing because the card issued to you by your bank is called a
 * <em>debit</em> card. However, this is because of how the
 * <strong>bank</strong>
 * carries your account on <em>their</em> books&hellip;</dd>
 * <dt><strong><em>Credit</em></strong>:</dt>
 * <dd>An account designated as a <em>credit</em> account has its balance
 * <strong>increased</strong> by credits to it, and <strong>decreased</strong>
 * by debits to it.<br>
 * <br>
 * As a real-world example, your checking account is carried on the books by
 * your bank as a <em>credit</em> account. This is because your checking account
 * is a
 * <strong><em>liability</em></strong> to the bank because that money belongs to
 * you. Therefore, whenever you debit your account with a deposit, it reduces
 * the banks bottom line because they no owe you that money. This is why the
 * card the bank issues to you is called a <em>debit</em> card&hellip;whenever
 * you use it, it debits your account on their books, thereby
 * <em>increasing</em> their financial strength because that is that much
 * <strong>less</strong> money they owe to you.</dd>
 * </dl>
 * <p>
 * In the table below, each of the five account types are described by their
 * names and what effect debits and credits have on them. This will help you to
 * have a little bit stronger grasp on this concept.
 * <br>
 * <table>
 * <caption>The Five (5) Accounting System Account Types</caption>
 * <tr>
 * <th>Account Type</th><th>Debit Effect</th><th>Credit Effect</th></tr>
 * <tr>
 * <td>Assets</td><td>Increases</td><td>Decreases</tr>
 * <tr>
 * <td>Liabilities</td><td>Decreases</td><td>Increases</tr>
 * <tr>
 * <td>Expenses</td><td>Increases</td><td>Decreases</tr>
 * <tr>
 * <td>Income</td><td>Decreases</td><td>Increases</tr>
 * <tr>
 * <td>Equity</td><td>Decreases</td><td>Increases</tr>
 * </table>
 * <p>
 * For a more detailed discussion of this topic, see
 * <a href="https://gitlab.com/pekinsoft-systems/ntos/-/wikis/Debit-and-Credit">
 * Accounting Primer: Debit and Credit</a>
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum AccountType {

    /**
     * Accounts that increase the value of the company and are carried as
     * <em>debit</em> accounts on the company's books.
     */
    ASSETS("Assets"),
    /**
     * Accounts that decrease the value of the company and are carried as
     * <em>credit</em> accounts on the company's books.
     */
    LIABILITIES("Liabilities"),
    /**
     * Accounts that decrease the current assets of the company and are carried
     * as <em>debit</em> accounts on the company's books.
     */
    EXPENSES("Expenses"),
    /**
     * Accounts that increase the current value of the company and are carried
     * as <em>credit</em> accounts on the company's books.
     */
    INCOME("Income"),
    /**
     * Accounts that increase the current value of the company and are carried
     * as <em>credit</em> accounts on the company's books.
     */
    EQUITY("Equity");

    private final String type;

    private AccountType(String type) {
        this.type = type;
    }

    /**
     * Retrieves the string value of this enumerated constant, i.e., if called
     * on the constant {@code AccountType.ASSETS}, the returned value is
     * "Assets".
     *
     * @return the string value representation of this {@code AccountType}
     * constant
     */
    @Override
    public String toString() {
        return type;
    }

    /**
     * Creates an {@code AccountType} constant of the given string value. This
     * method is mostly for use when a <em>Persistence Entity</em> is reading
     * the value from the database, in order to store the enumeration constant
     * in an entity field for the application.
     *
     * @param type the string value of the enumeration constant type
     * @return the {@code AccountType} enumeration constant represented by the
     * string value supplied
     * @throws IllegalArgumentException if the specified {@code type} is not a
     * known {@code AccountType} constant
     */
    public static final AccountType of(String type) {
        switch (type.toLowerCase()) {
            case "assets":
                return ASSETS;
            case "liabilities":
                return LIABILITIES;
            case "expenses":
                return EXPENSES;
            case "income":
                return INCOME;
            case "equity":
                return EQUITY;
            default:
                throw new IllegalArgumentException("Unknown account type");
        }
    }
}
