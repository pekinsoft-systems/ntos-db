/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   AssetStatus.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code AssetStatus} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum AssetStatus {

    IN_SERVICE("In Service"),
    OUT_OF_SERVICE("Out of Service"),
    DOWN_AWAITING_REPAIR("Down: Awaiting Repair"),
    DOWN_AWAITING_PARTS("Down: Awaiting Parts"),
    SOLD("Sold");

    private final String status;

    private AssetStatus(String status) {
        this.status = status;
    }

    /**
     * Retrieves the {@code AssetStatus} enumerated constant as a string
     * representation. For example,
     * {@code AssetStatus.DOWN_AWAITING_PARTS.toString()} returns the string "Down:
     * Awaiting Parts".
     *
     * @return the string representation of the {@code AssetStatus} enumerated
     * constant
     */
    @Override
    public String toString() {
        return status;
    }

    /**
     * Retrieves the {@code AssetStatus} enumerated constant from the specified
     * string value. This method is typically only used by the {@link
     * com.pekinsoft.ntos.db.converters.StatusConverter StatusConverter} for
     * retrieving the data from the database.
     *
     * @param status the string value representation of the {@code AssetStatus}
     * enumerated constant desired
     * @return the {@code AssetStatus} enumerated constant denoted by the specified
     * string value
     * @throws IllegalArgumentException if the specified {@code status}
     * parameter is an unknown status
     */
    public static final AssetStatus of(String status) {
        switch (status.toLowerCase()) {
            case "in service":
                return IN_SERVICE;
            case "out of service":
                return OUT_OF_SERVICE;
            case "down: awaiting repair":
                return DOWN_AWAITING_REPAIR;
            case "down: awaiting parts":
                return DOWN_AWAITING_PARTS;
            case "sold":
                return SOLD;
            default:
                throw new IllegalArgumentException("Unknown status.");
        }
    }

}
