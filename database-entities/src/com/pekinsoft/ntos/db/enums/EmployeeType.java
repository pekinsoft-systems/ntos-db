/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   EmployeeType.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db.enums;

/**
 * The {@code EmployeeType} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public enum EmployeeType {

    FULL_TIME_HOURLY("Full-Time: Hourly"),
    FULL_TIME_MILEAGE("Full-Time: Mileage"),
    FULL_TIME_SALARY_EXEMPT("Full-Time: Salary (Exempt)"),
    FULL_TIME_SALARY_NON_EXEMPT("Full-time: Salary (Non-Exempt)"),
    IC("Independent Contractor"),
    PART_TIME_HOURLY("Part-Time: Hourly"),
    PART_TIME_MILEAGE("Part-Time: Mileage"),
    SEASONAL_HOURLY("Seasonal: Hourly"),
    SEASONAL_MILEAGE("Seasonal: Mileage"),
    SEASONAL_SALARY_EXEMPT("Seasonal: Salary (Exempt)"),
    SEASONAL_SALARY_NON_EXEMPT("Seasonal: Salary (Non-Exempt)");

    private final String type;

    private EmployeeType(String type) {
        this.type = type;
    }

    /**
     * Retrieves the string value of this enumerated constant, i.e., if called
     * on the constant {@code EmployeeType.PART_TIME_HOURLY}, the returned value
     * is "Part-Time: Hourly".
     *
     * @return the string value representation of this {@code EmployeeType}
     * constant
     */
    @Override
    public String toString() {
        return type;
    }

    /**
     * Creates an {@code EmployeeType} constant of the given string value. This
     * method is mostly for use when a <em>Persistence Entity</em> is reading
     * the value from the database, in order to store the enumeration constant
     * in an entity field for the application.
     *
     * @param type the string value of the enumeration constant type
     * @return the {@code EmployeeType} enumeration constant represented by the
     * string value supplied
     * @throws IllegalArgumentException if the specified {@code type} is not a
     * known {@code EmployeeType} constant
     */
    public static final EmployeeType of(String type) {
        switch (type.toLowerCase()) {
            case "full-time: hourly":
                return FULL_TIME_HOURLY;
            case "full-time: mileage":
                return FULL_TIME_MILEAGE;
            case "full-time: salary (exempt)":
                return FULL_TIME_SALARY_EXEMPT;
            case "full-time: salary (non-exempt)":
                return FULL_TIME_SALARY_NON_EXEMPT;
            case "independent contractor":
                return IC;
            case "part-time: hourly":
                return PART_TIME_HOURLY;
            case "part-time: mileage":
                return PART_TIME_MILEAGE;
            case "seasonal: hourly":
                return SEASONAL_HOURLY;
            case "seasonal: mileage":
                return SEASONAL_MILEAGE;
            case "seasonal: salary (exempt)":
                return SEASONAL_SALARY_EXEMPT;
            case "seasonal: salary (non-exempt)":
                return SEASONAL_SALARY_NON_EXEMPT;
            default:
                throw new IllegalArgumentException("Unknown employee type: " + type);
        }
    }

}
