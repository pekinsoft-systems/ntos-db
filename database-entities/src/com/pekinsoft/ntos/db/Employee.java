/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Employee.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.EmployeeType;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The {@code Employee} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Employee.findAll",
            query = "SELECT e FROM Employee e"),
    @NamedQuery(name = "Employee.findBySsn",
            query = "SELECT e FROM Employee e WHERE e.ssn = :ssn"),
    @NamedQuery(name = "Employee.findByLastName",
            query = "SELECT e FROM Employee e WHERE e.lastName = :lastName"),
    @NamedQuery(name = "Employee.findByFirstName",
            query = "SELECT e FROM Employee e WHERE e.firstName = :firstName"),
    @NamedQuery(name = "Employee.findByMiddleName",
            query = "SELECT e FROM Employee e WHERE e.middleName = :middleName"),
    @NamedQuery(name = "Employee.findByStreet",
            query = "SELECT e FROM Employee e WHERE e.street = :street"),
    @NamedQuery(name = "Employee.findByApartment",
            query = "SELECT e FROM Employee e WHERE e.apartment = :apartment"),
    @NamedQuery(name = "Employee.findByCity",
            query = "SELECT e FROM Employee e WHERE e.city = :city"),
    @NamedQuery(name = "Employee.findByStateOrProvince",
            query = "SELECT e FROM Employee e WHERE e.stateOrProvince = :stateOrProvince"),
    @NamedQuery(name = "Employee.findByPostalCode",
            query = "SELECT e FROM Employee e WHERE e.postalCode = :postalCode"),
    @NamedQuery(name = "Employee.findByPhone",
            query = "SELECT e FROM Employee e WHERE e.phone = :phone"),
    @NamedQuery(name = "Employee.findByPersonalEmail",
            query = "SELECT e FROM Employee e WHERE e.personalEmail = :personalEmail"),
    @NamedQuery(name = "Employee.findByBirthDate",
            query = "SELECT e FROM Employee e WHERE e.birthDate = :birthDate"),
    @NamedQuery(name = "Employee.findByHireDate",
            query = "SELECT e FROM Employee e WHERE e.hireDate = :hireDate"),
    @NamedQuery(name = "Employee.findByReviewDate",
            query = "SELECT e FROM Employee e WHERE e.reviewDate = :reviewDate"),
    @NamedQuery(name = "Employee.findByTerminationDate",
            query = "SELECT e FROM Employee e WHERE e.terminationDate = :terminationDate"),
    @NamedQuery(name = "Employee.findByEmergencyContact",
            query = "SELECT e FROM Employee e WHERE e.emergencyContact = :emergencyContact"),
    @NamedQuery(name = "Employee.findByEmergencyContactPhone",
            query = "SELECT e FROM Employee e WHERE e.emergencyContactPhone = :emergencyContactPhone"),
    @NamedQuery(name = "Employee.findByEmergencyContactRelation",
            query = "SELECT e FROM Employee e WHERE e.emergencyContactRelation = :emergencyContactRelation"),
    @NamedQuery(name = "Employee.findByMarried",
            query = "SELECT e FROM Employee e WHERE e.married = :married"),
    @NamedQuery(name = "Employee.findByEmploymentType",
            query = "SELECT e FROM Employee e WHERE e.employmentType = :employmentType"),
    @NamedQuery(name = "Employee.findByInactive",
            query = "SELECT e FROM Employee e WHERE e.inactive = :inactive")})
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final DateTimeFormatter FORMATTER
            = DateTimeFormatter.ISO_LOCAL_DATE;

    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 11)
    private String ssn;
    @Basic(optional = false)
    @Column(name = "LAST_NAME", nullable = false, length = 30)
    private String lastName;
    @Basic(optional = false)
    @Column(name = "FIRST_NAME", nullable = false, length = 25)
    private String firstName;
    @Basic(optional = false)
    @Column(name = "MIDDLE_NAME", nullable = false, length = 20)
    private String middleName;
    @Basic(optional = false)
    @Column(nullable = false, length = 40)
    private String street;
    @Column(length = 25)
    private String apartment;
    @Basic(optional = false)
    @Column(nullable = false, length = 40)
    private String city;
    @Basic(optional = false)
    @Column(name = "STATE_OR_PROVINCE", nullable = false, length = 2)
    private String stateOrProvince;
    @Basic(optional = false)
    @Column(name = "POSTAL_CODE", nullable = false, length = 15)
    private String postalCode;
    @Basic(optional = false)
    @Column(nullable = false, length = 14)
    private String phone;
    @Column(name = "PERSONAL_EMAIL", length = 40)
    private String personalEmail;
    @Basic(optional = false)
    @Column(name = "BIRTH_DATE", nullable = false, length = 30)
    private LocalDate birthDate;
    @Basic(optional = false)
    @Column(name = "HIRE_DATE", nullable = false, length = 30)
    private LocalDate hireDate;
    @Basic(optional = false)
    @Column(name = "REVIEW_DATE", nullable = false, length = 30)
    private LocalDate reviewDate;
    @Column(name = "TERMINATION_DATE", length = 30)
    private LocalDate terminationDate;
    @Basic(optional = false)
    @Column(name = "EMERGENCY_CONTACT", nullable = false, length = 50)
    private String emergencyContact;
    @Basic(optional = false)
    @Column(name = "EMERGENCY_CONTACT_PHONE", nullable = false, length = 14)
    private String emergencyContactPhone;
    @Basic(optional = false)
    @Column(name = "EMERGENCY_CONTACT_RELATION", nullable = false, length = 20)
    private String emergencyContactRelation;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean married;
    @Basic(optional = false)
    @Column(name = "EMPLOYMENT_TYPE", nullable = false, length = 25)
    private EmployeeType employmentType;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean inactive;
    @JoinColumn(name = "SSN", referencedColumnName = "SSN", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false)
    private EmployeeTaxStatus employeeTaxStatus;

    public Employee() {

    }

    public Employee(String ssn) {
        this.ssn = ssn;
    }

    public Employee(String ssn, String lastName, String firstName,
            String middleName, String street, String city,
            String stateOrProvince, String postalCode, String phone,
            LocalDate birthDate, LocalDate hireDate, LocalDate reviewDate,
            String emergencyContact, String emergencyContactPhone,
            String emergencyContactRelation, Boolean married,
            EmployeeType employmentType, Boolean inactive) {
        this.ssn = ssn;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.street = street;
        this.city = city;
        this.stateOrProvince = stateOrProvince;
        this.postalCode = postalCode;
        this.phone = phone;
        this.birthDate = birthDate;
        this.hireDate = hireDate;
        this.reviewDate = reviewDate;
        this.emergencyContact = emergencyContact;
        this.emergencyContactPhone = emergencyContactPhone;
        this.emergencyContactRelation = emergencyContactRelation;
        this.married = married;
        this.employmentType = employmentType;
        this.inactive = inactive;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStateOrProvince() {
        return stateOrProvince;
    }

    public void setStateOrProvince(String stateOrProvince) {
        this.stateOrProvince = stateOrProvince;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPersonalEmail() {
        return personalEmail;
    }

    public void setPersonalEmail(String personalEmail) {
        this.personalEmail = personalEmail;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getHireDate() {
        return hireDate;
    }

    public void setHireDate(LocalDate hireDate) {
        this.hireDate = hireDate;
    }

    public LocalDate getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(LocalDate reviewDate) {
        this.reviewDate = reviewDate;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getEmergencyContactRelation() {
        return emergencyContactRelation;
    }

    public void setEmergencyContactRelation(String emergencyContactRelation) {
        this.emergencyContactRelation = emergencyContactRelation;
    }

    public Boolean getMarried() {
        return married;
    }

    public void setMarried(Boolean married) {
        this.married = married;
    }

    public EmployeeType getEmploymentType() {
        return employmentType;
    }

    public void setEmploymentType(EmployeeType employmentType) {
        this.employmentType = employmentType;
    }

    public Boolean getInactive() {
        return inactive;
    }

    public void setInactive(Boolean inactive) {
        this.inactive = inactive;
    }

    public EmployeeTaxStatus getEmployeeTaxStatus() {
        return employeeTaxStatus;
    }

    public void setEmployeeTaxStatus(EmployeeTaxStatus employeeTaxStatus) {
        this.employeeTaxStatus = employeeTaxStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ssn != null ? ssn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Employee)) {
            return false;
        }
        Employee other = (Employee) object;
        return !((this.ssn == null && other.ssn != null)
                || (this.ssn != null && !this.ssn.equals(other.ssn)));
    }

    @Override
    public String toString() {
        String taxStatus;
        if (employeeTaxStatus != null) {
            if (employmentType == EmployeeType.IC) {
                taxStatus = "; IRS Form 1099 on File";
            } else {
                taxStatus = "; IRS Form W4 on File";
            }
        } else {
            taxStatus = "; No Tax Filing Status on File";
        }
        return employmentType.toString() + ": " + lastName + ", " + firstName
                + (middleName != null && !middleName.trim().isEmpty()
                ? " " + middleName + " [ Hired: "
                : " [ Hired: ")
                + FORMATTER.format(hireDate) + taxStatus + " ]";
    }

}
