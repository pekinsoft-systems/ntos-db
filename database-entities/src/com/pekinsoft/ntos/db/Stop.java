/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Stop.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The {@code Stop} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "STOPS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Stop.findAll",
            query = "SELECT s FROM Stop s"),
    @NamedQuery(name = "Stop.findById",
            query = "SELECT s FROM Stop s WHERE s.id = :id"),
    @NamedQuery(name = "Stop.findByStopNumber",
            query = "SELECT s FROM Stop s WHERE s.stopNumber = :stopNumber"),
    @NamedQuery(name = "Stop.findByEarlyDate",
            query = "SELECT s FROM Stop s WHERE s.earlyDate = :earlyDate"),
    @NamedQuery(name = "Stop.findByEarlyTime",
            query = "SELECT s FROM Stop s WHERE s.earlyTime = :earlyTime"),
    @NamedQuery(name = "Stop.findByLateDate",
            query = "SELECT s FROM Stop s WHERE s.lateDate = :lateDate"),
    @NamedQuery(name = "Stop.findByLateTime",
            query = "SELECT s FROM Stop s WHERE s.lateTime = :lateTime"),
    @NamedQuery(name = "Stop.findByArrivalDate",
            query = "SELECT s FROM Stop s WHERE s.arrivalDate = :arrivalDate"),
    @NamedQuery(name = "Stop.findByArrivalTime",
            query = "SELECT s FROM Stop s WHERE s.arrivalTime = :arrivalTime"),
    @NamedQuery(name = "Stop.findByDepartureDate",
            query = "SELECT s FROM Stop s WHERE s.departureDate = :departureDate"),
    @NamedQuery(name = "Stop.findByDepartureTime",
            query = "SELECT s FROM Stop s WHERE s.departureTime = :departureTime"),
    @NamedQuery(name = "Stop.findByDelivery",
            query = "SELECT s FROM Stop s WHERE s.delivery = :delivery")})
public class Stop implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(nullable = false)
    private Long id;
    @Basic(optional = false)
    @Column(name = "STOP_NUMBER", nullable = false)
    private int stopNumber;
    @Basic(optional = false)
    @Column(name = "EARLY_DATE", nullable = false, length = 30)
    private LocalDate earlyDate;
    @Basic(optional = false)
    @Column(name = "EARLY_TIME", nullable = false, length = 15)
    private LocalTime earlyTime;
    @Basic(optional = false)
    @Column(name = "LATE_DATE", nullable = false, length = 30)
    private LocalDate lateDate;
    @Basic(optional = false)
    @Column(name = "LATE_TIME", nullable = false, length = 15)
    private LocalTime lateTime;
    @Column(name = "ARRIVAL_DATE", length = 30)
    private LocalDate arrivalDate;
    @Column(name = "ARRIVAL_TIME", length = 15)
    private LocalTime arrivalTime;
    @Column(name = "DEPARTURE_DATE", length = 30)
    private LocalDate departureDate;
    @Column(name = "DEPARTURE_TIME", length = 15)
    private LocalTime departureTime;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean delivery;
    @JoinColumn(name = "CUSTOMER_ID", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Customer customerId;
    @JoinColumn(name = "ORDER_NUMBER", referencedColumnName = "ORDER_NUMBER", nullable = false)
    @ManyToOne(optional = false)
    private Load orderNumber;

    public Stop() {

    }

    public Stop(Long id) {
        this.id = id;
    }

    public Stop(Long id, int stopNumber, LocalDate earlyDate, LocalTime earlyTime,
            LocalDate lateDate, LocalTime lateTime, Boolean delivery) {
        this.id = id;
        this.stopNumber = stopNumber;
        this.earlyDate = earlyDate;
        this.earlyTime = earlyTime;
        this.lateDate = lateDate;
        this.lateTime = lateTime;
        this.delivery = delivery;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStopNumber() {
        return stopNumber;
    }

    public void setStopNumber(int stopNumber) {
        this.stopNumber = stopNumber;
    }

    public LocalDate getEarlyDate() {
        return earlyDate;
    }

    public void setEarlyDate(LocalDate earlyDate) {
        this.earlyDate = earlyDate;
    }

    public LocalTime getEarlyTime() {
        return earlyTime;
    }

    public void setEarlyTime(LocalTime earlyTime) {
        this.earlyTime = earlyTime;
    }

    public LocalDate getLateDate() {
        return lateDate;
    }

    public void setLateDate(LocalDate lateDate) {
        this.lateDate = lateDate;
    }

    public LocalTime getLateTime() {
        return lateTime;
    }

    public void setLateTime(LocalTime lateTime) {
        this.lateTime = lateTime;
    }

    public LocalDate getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(LocalDate arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public LocalDate getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(LocalDate departureDate) {
        this.departureDate = departureDate;
    }

    public LocalTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalTime departureTime) {
        this.departureTime = departureTime;
    }

    public Boolean getDelivery() {
        return delivery;
    }

    public void setDelivery(Boolean delivery) {
        this.delivery = delivery;
    }

    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    public Load getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Load orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Stop)) {
            return false;
        }
        Stop other = (Stop) object;
        return !((this.id == null && other.id != null)
                || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        String earlyD = DateTimeFormatter.ISO_LOCAL_DATE.format(earlyDate);
        String earlyT = DateTimeFormatter.ISO_LOCAL_TIME.format(earlyTime);
        String lateD = DateTimeFormatter.ISO_LOCAL_DATE.format(lateDate);
        String lateT = DateTimeFormatter.ISO_LOCAL_TIME.format(lateTime);
        String company = customerId.getCompany();
        String location = customerId.getCity() + ", "
                + customerId.getStateOrProvince();
        String reason;
        String order = "[ " + orderNumber + " ] ";
        if (delivery) {
            reason = "Delivering to ";
        } else {
            reason = "Picking up at ";
        }
        return reason + company + location + "from: " + earlyD + "@" + earlyT
                + ", until: " + lateD + "@" + lateT;
    }

}
