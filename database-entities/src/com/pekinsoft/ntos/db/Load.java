/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Load.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.Commodity;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The {@code Load} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "LOADS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Load.findAll",
            query = "SELECT l FROM Load l"),
    @NamedQuery(name = "Load.findByOrderNumber",
            query = "SELECT l FROM Load l WHERE l.orderNumber = :orderNumber"),
    @NamedQuery(name = "Load.findByTripNumber",
            query = "SELECT l FROM Load l WHERE l.tripNumber = :tripNumber"),
    @NamedQuery(name = "Load.findByGrossRevenue",
            query = "SELECT l FROM Load l WHERE l.grossRevenue = :grossRevenue"),
    @NamedQuery(name = "Load.findByPaidDistance",
            query = "SELECT l FROM Load l WHERE l.paidDistance = :paidDistance"),
    @NamedQuery(name = "Load.findByDeadheadDistance",
            query = "SELECT l FROM Load l WHERE l.deadheadDistance = :deadheadDistance"),
    @NamedQuery(name = "Load.findByDateBooked",
            query = "SELECT l FROM Load l WHERE l.dateBooked = :dateBooked"),
    @NamedQuery(name = "Load.findByDateDispatched",
            query = "SELECT l FROM Load l WHERE l.dateDispatched = :dateDispatched"),
    @NamedQuery(name = "Load.findByCommodity",
            query = "SELECT l FROM Load l WHERE l.commodity = :commodity"),
    @NamedQuery(name = "Load.findByWeight",
            query = "SELECT l FROM Load l WHERE l.weight = :weight"),
    @NamedQuery(name = "Load.findByPieces",
            query = "SELECT l FROM Load l WHERE l.pieces = :pieces"),
    @NamedQuery(name = "Load.findByHeight",
            query = "SELECT l FROM Load l WHERE l.height = :height"),
    @NamedQuery(name = "Load.findByLength",
            query = "SELECT l FROM Load l WHERE l.length = :length"),
    @NamedQuery(name = "Load.findByWidth",
            query = "SELECT l FROM Load l WHERE l.width = :width"),
    @NamedQuery(name = "Load.findByHazmat",
            query = "SELECT l FROM Load l WHERE l.hazmat = :hazmat"),
    @NamedQuery(name = "Load.findByTarped",
            query = "SELECT l FROM Load l WHERE l.tarped = :tarped"),
    @NamedQuery(name = "Load.findByTarpHeight",
            query = "SELECT l FROM Load l WHERE l.tarpHeight = :tarpHeight"),
    @NamedQuery(name = "Load.findByTeam",
            query = "SELECT l FROM Load l WHERE l.team = :team"),
    @NamedQuery(name = "Load.findByTopCustomer",
            query = "SELECT l FROM Load l WHERE l.topCustomer = :topCustomer"),
    @NamedQuery(name = "Load.findByCbd",
            query = "SELECT l FROM Load l WHERE l.cbd = :cbd"),
    @NamedQuery(name = "Load.findByLtl",
            query = "SELECT l FROM Load l WHERE l.ltl = :ltl"),
    @NamedQuery(name = "Load.findByTwic",
            query = "SELECT l FROM Load l WHERE l.twic = :twic"),
    @NamedQuery(name = "Load.findByRamps",
            query = "SELECT l FROM Load l WHERE l.ramps = :ramps"),
    @NamedQuery(name = "Load.findByBolNumber",
            query = "SELECT l FROM Load l WHERE l.bolNumber = :bolNumber"),
    @NamedQuery(name = "Load.findByPickupNumber",
            query = "SELECT l FROM Load l WHERE l.pickupNumber = :pickupNumber"),
    @NamedQuery(name = "Load.findByReferenceNumber",
            query = "SELECT l FROM Load l WHERE l.referenceNumber = :referenceNumber")})
public class Load implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ORDER_NUMBER", nullable = false, length = 30)
    private String orderNumber;
    @Basic(optional = false)
    @Column(name = "TRIP_NUMBER", nullable = false, length = 30)
    private String tripNumber;
    @Basic(optional = false)
    @Column(name = "GROSS_REVENUE", nullable = false)
    private long grossRevenue;
    @Basic(optional = false)
    @Column(name = "PAID_DISTANCE", nullable = false)
    private int paidDistance;
    @Basic(optional = false)
    @Column(name = "DEADHEAD_DISTANCE", nullable = false)
    private int deadheadDistance;
    @Basic(optional = false)
    @Column(name = "DATE_BOOKED", nullable = false, length = 30)
    private LocalDate dateBooked;
    @Basic(optional = false)
    @Column(name = "DATE_DISPATCHED", nullable = false, length = 30)
    private LocalDate dateDispatched;
    @Basic(optional = false)
    @Column(nullable = false, length = 40)
    private Commodity commodity;
    @Basic(optional = false)
    @Column(nullable = false)
    private int weight;
    @Basic(optional = false)
    @Column(nullable = false)
    private int pieces;
    @Basic(optional = false)
    @Column(nullable = false)
    private int height;
    @Basic(optional = false)
    @Column(nullable = false)
    private int length;
    @Basic(optional = false)
    @Column(nullable = false)
    private int width;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean hazmat;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean tarped;
    @Basic(optional = false)
    @Column(name = "TARP_HEIGHT", nullable = false)
    private int tarpHeight;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean team;
    @Basic(optional = false)
    @Column(name = "TOP_CUSTOMER", nullable = false)
    private Boolean topCustomer;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean cbd;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean ltl;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean twic;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean ramps;
    @Basic(optional = false)
    @Column(name = "BOL_NUMBER", nullable = false, length = 40)
    private String bolNumber;
    @Column(name = "PICKUP_NUMBER", length = 30)
    private String pickupNumber;
    @Column(name = "REFERENCE_NUMBER", length = 30)
    private String referenceNumber;
    @Lob
    @Column(name = "LOAD_NOTES", length = 32700)
    private String loadNotes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orderNumber")
    private List<Stop> stopList;
    @JoinColumn(name = "ASSIGNED_UNIT", referencedColumnName = "UNIT_NUMBER", nullable = false)
    @ManyToOne(optional = false)
    private Asset assignedUnit;

    public Load() {

    }

    public Load(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Load(String orderNumber, String tripNumber, long grossRevenue,
            int paidDistance, int deadheadDistance, LocalDate dateBooked,
            LocalDate dateDispatched, Commodity commodity, int weight, int pieces,
            int height, int length, int width, Boolean hazmat, Boolean tarped,
            int tarpHeight, Boolean team, Boolean topCustomer, Boolean cbd,
            Boolean ltl, Boolean twic, Boolean ramps, String bolNumber) {
        this.orderNumber = orderNumber;
        this.tripNumber = tripNumber;
        this.grossRevenue = grossRevenue;
        this.paidDistance = paidDistance;
        this.deadheadDistance = deadheadDistance;
        this.dateBooked = dateBooked;
        this.dateDispatched = dateDispatched;
        this.commodity = commodity;
        this.weight = weight;
        this.pieces = pieces;
        this.height = height;
        this.length = length;
        this.width = width;
        this.hazmat = hazmat;
        this.tarped = tarped;
        this.tarpHeight = tarpHeight;
        this.team = team;
        this.topCustomer = topCustomer;
        this.cbd = cbd;
        this.ltl = ltl;
        this.twic = twic;
        this.ramps = ramps;
        this.bolNumber = bolNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTripNumber() {
        return tripNumber;
    }

    public void setTripNumber(String tripNumber) {
        this.tripNumber = tripNumber;
    }

    public long getGrossRevenue() {
        return grossRevenue;
    }

    public void setGrossRevenue(long grossRevenue) {
        this.grossRevenue = grossRevenue;
    }

    public int getPaidDistance() {
        return paidDistance;
    }

    public void setPaidDistance(int paidDistance) {
        this.paidDistance = paidDistance;
    }

    public int getDeadheadDistance() {
        return deadheadDistance;
    }

    public void setDeadheadDistance(int deadheadDistance) {
        this.deadheadDistance = deadheadDistance;
    }

    public LocalDate getDateBooked() {
        return dateBooked;
    }

    public void setDateBooked(LocalDate dateBooked) {
        this.dateBooked = dateBooked;
    }

    public LocalDate getDateDispatched() {
        return dateDispatched;
    }

    public void setDateDispatched(LocalDate dateDispatched) {
        this.dateDispatched = dateDispatched;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPieces() {
        return pieces;
    }

    public void setPieces(int pieces) {
        this.pieces = pieces;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Boolean getHazmat() {
        return hazmat;
    }

    public void setHazmat(Boolean hazmat) {
        this.hazmat = hazmat;
    }

    public Boolean getTarped() {
        return tarped;
    }

    public void setTarped(Boolean tarped) {
        this.tarped = tarped;
    }

    public int getTarpHeight() {
        return tarpHeight;
    }

    public void setTarpHeight(int tarpHeight) {
        this.tarpHeight = tarpHeight;
    }

    public Boolean getTeam() {
        return team;
    }

    public void setTeam(Boolean team) {
        this.team = team;
    }

    public Boolean getTopCustomer() {
        return topCustomer;
    }

    public void setTopCustomer(Boolean topCustomer) {
        this.topCustomer = topCustomer;
    }

    public Boolean getCbd() {
        return cbd;
    }

    public void setCbd(Boolean cbd) {
        this.cbd = cbd;
    }

    public Boolean getLtl() {
        return ltl;
    }

    public void setLtl(Boolean ltl) {
        this.ltl = ltl;
    }

    public Boolean getTwic() {
        return twic;
    }

    public void setTwic(Boolean twic) {
        this.twic = twic;
    }

    public Boolean getRamps() {
        return ramps;
    }

    public void setRamps(Boolean ramps) {
        this.ramps = ramps;
    }

    public String getBolNumber() {
        return bolNumber;
    }

    public void setBolNumber(String bolNumber) {
        this.bolNumber = bolNumber;
    }

    public String getPickupNumber() {
        return pickupNumber;
    }

    public void setPickupNumber(String pickupNumber) {
        this.pickupNumber = pickupNumber;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getLoadNotes() {
        return loadNotes;
    }

    public void setLoadNotes(String loadNotes) {
        this.loadNotes = loadNotes;
    }

    public List<Stop> getStopList() {
        return stopList;
    }

    public void setStopList(List<Stop> stopList) {
        this.stopList = stopList;
    }

    public Asset getAssignedUnit() {
        return assignedUnit;
    }

    public void setAssignedUnit(Asset assignedUnit) {
        this.assignedUnit = assignedUnit;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderNumber != null ? orderNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Load)) {
            return false;
        }
        Load other = (Load) object;
        return !((this.orderNumber == null && other.orderNumber != null)
                || (this.orderNumber != null
                && !this.orderNumber.equals(other.orderNumber)));
    }

    @Override
    public String toString() {
        String start = "";
        String finish = "";
        String from = "";
        String pickup = "";
        String to = "";
        String deliver = "";
        int totalStops = stopList.size();
        for (Stop s : stopList) {
            if (s.getStopNumber() == 1) {
                start = " on "
                        + DateTimeFormatter.ISO_LOCAL_DATE.format(s.getEarlyDate())
                        + " ";
                from = s.getCustomerId().getCompany();
                pickup = " (" + s.getCustomerId().getCity() + ", "
                        + s.getCustomerId().getStateOrProvince() + ")";
            } else if (s.getStopNumber() == totalStops) {
                finish = " by "
                        + DateTimeFormatter.ISO_LOCAL_DATE.format(s.getLateDate())
                        + " ";
                to = s.getCustomerId().getCompany();
                deliver = " (" + s.getCustomerId().getCity() + ", "
                        + s.getCustomerId().getStateOrProvince() + ")";
            }
        }
        return "[ " + orderNumber + " ] From " + from + pickup + start + " to "
                + to + deliver + finish + " hauling " + commodity.toString();
    }

}
