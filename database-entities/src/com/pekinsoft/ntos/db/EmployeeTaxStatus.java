/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   EmployeeTaxStatus.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.TaxStatus;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The {@code EmployeeTaxStatus} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "EMPLOYEE_TAX_STATUS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "EmployeeTaxStatus.findAll",
            query = "SELECT e FROM EmployeeTaxStatus e"),
    @NamedQuery(name = "EmployeeTaxStatus.findBySsn",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.ssn = :ssn"),
    @NamedQuery(name = "EmployeeTaxStatus.findByFederalStatus",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.federalStatus = :federalStatus"),
    @NamedQuery(name = "EmployeeTaxStatus.findByFederalAllowance",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.federalAllowance = :federalAllowance"),
    @NamedQuery(name = "EmployeeTaxStatus.findByFederalAdditional",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.federalAdditional = :federalAdditional"),
    @NamedQuery(name = "EmployeeTaxStatus.findByStateStatus",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.stateStatus = :stateStatus"),
    @NamedQuery(name = "EmployeeTaxStatus.findByStateAllowance",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.stateAllowance = :stateAllowance"),
    @NamedQuery(name = "EmployeeTaxStatus.findByStateAdditional",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.stateAdditional = :stateAdditional"),
    @NamedQuery(name = "EmployeeTaxStatus.findByStateW2Id",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.stateW2Id = :stateW2Id"),
    @NamedQuery(name = "EmployeeTaxStatus.findByLocalStatus",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.localStatus = :localStatus"),
    @NamedQuery(name = "EmployeeTaxStatus.findByLocalAllowance",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.localAllowance = :localAllowance"),
    @NamedQuery(name = "EmployeeTaxStatus.findByLocalAdditional",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.localAdditional = :localAdditional"),
    @NamedQuery(name = "EmployeeTaxStatus.findByLocalW2Id",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.localW2Id = :localW2Id"),
    @NamedQuery(name = "EmployeeTaxStatus.findByPension",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.pension = :pension"),
    @NamedQuery(name = "EmployeeTaxStatus.findByDefered",
            query = "SELECT e FROM EmployeeTaxStatus e WHERE e.defered = :defered")})
public class EmployeeTaxStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false, length = 11)
    private String ssn;
    @Basic(optional = false)
    @Column(name = "FEDERAL_STATUS", nullable = false, length = 25)
    private TaxStatus federalStatus;
    @Basic(optional = false)
    @Column(name = "FEDERAL_ALLOWANCE", nullable = false)
    private int federalAllowance;
    @Basic(optional = false)
    @Column(name = "FEDERAL_ADDITIONAL", nullable = false)
    private int federalAdditional;
    @Basic(optional = false)
    @Column(name = "STATE_STATUS", nullable = false, length = 25)
    private TaxStatus stateStatus;
    @Basic(optional = false)
    @Column(name = "STATE_ALLOWANCE", nullable = false)
    private int stateAllowance;
    @Basic(optional = false)
    @Column(name = "STATE_ADDITIONAL", nullable = false)
    private int stateAdditional;
    @Basic(optional = false)
    @Column(name = "STATE_W2_ID", nullable = false, length = 2)
    private String stateW2Id;
    @Basic(optional = false)
    @Column(name = "LOCAL_STATUS", nullable = false, length = 25)
    private TaxStatus localStatus;
    @Basic(optional = false)
    @Column(name = "LOCAL_ALLOWANCE", nullable = false)
    private int localAllowance;
    @Basic(optional = false)
    @Column(name = "LOCAL_ADDITIONAL", nullable = false)
    private int localAdditional;
    @Basic(optional = false)
    @Column(name = "LOCAL_W2_ID", nullable = false, length = 25)
    private String localW2Id;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean pension;
    @Basic(optional = false)
    @Column(nullable = false)
    private Boolean defered;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "employeeTaxStatus")
    private Employee employee;

    public EmployeeTaxStatus() {

    }

    public EmployeeTaxStatus(String ssn) {
        this.ssn = ssn;
    }

    public EmployeeTaxStatus(String ssn, TaxStatus federalStatus,
            int federalAllowance, int federalAdditional, TaxStatus stateStatus,
            int stateAllowance, int stateAdditional, String stateW2Id,
            TaxStatus localStatus, int localAllowance, int localAdditional,
            String localW2Id, Boolean pension, Boolean defered) {
        this.ssn = ssn;
        this.federalStatus = federalStatus;
        this.federalAllowance = federalAllowance;
        this.federalAdditional = federalAdditional;
        this.stateStatus = stateStatus;
        this.stateAllowance = stateAllowance;
        this.stateAdditional = stateAdditional;
        this.stateW2Id = stateW2Id;
        this.localStatus = localStatus;
        this.localAllowance = localAllowance;
        this.localAdditional = localAdditional;
        this.localW2Id = localW2Id;
        this.pension = pension;
        this.defered = defered;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public TaxStatus getFederalStatus() {
        return federalStatus;
    }

    public void setFederalStatus(TaxStatus federalStatus) {
        this.federalStatus = federalStatus;
    }

    public int getFederalAllowance() {
        return federalAllowance;
    }

    public void setFederalAllowance(int federalAllowance) {
        this.federalAllowance = federalAllowance;
    }

    public int getFederalAdditional() {
        return federalAdditional;
    }

    public void setFederalAdditional(int federalAdditional) {
        this.federalAdditional = federalAdditional;
    }

    public TaxStatus getStateStatus() {
        return stateStatus;
    }

    public void setStateStatus(TaxStatus stateStatus) {
        this.stateStatus = stateStatus;
    }

    public int getStateAllowance() {
        return stateAllowance;
    }

    public void setStateAllowance(int stateAllowance) {
        this.stateAllowance = stateAllowance;
    }

    public int getStateAdditional() {
        return stateAdditional;
    }

    public void setStateAdditional(int stateAdditional) {
        this.stateAdditional = stateAdditional;
    }

    public String getStateW2Id() {
        return stateW2Id;
    }

    public void setStateW2Id(String stateW2Id) {
        this.stateW2Id = stateW2Id;
    }

    public TaxStatus getLocalStatus() {
        return localStatus;
    }

    public void setLocalStatus(TaxStatus localStatus) {
        this.localStatus = localStatus;
    }

    public int getLocalAllowance() {
        return localAllowance;
    }

    public void setLocalAllowance(int localAllowance) {
        this.localAllowance = localAllowance;
    }

    public int getLocalAdditional() {
        return localAdditional;
    }

    public void setLocalAdditional(int localAdditional) {
        this.localAdditional = localAdditional;
    }

    public String getLocalW2Id() {
        return localW2Id;
    }

    public void setLocalW2Id(String localW2Id) {
        this.localW2Id = localW2Id;
    }

    public Boolean getPension() {
        return pension;
    }

    public void setPension(Boolean pension) {
        this.pension = pension;
    }

    public Boolean getDefered() {
        return defered;
    }

    public void setDefered(Boolean defered) {
        this.defered = defered;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ssn != null ? ssn.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof EmployeeTaxStatus)) {
            return false;
        }
        EmployeeTaxStatus other = (EmployeeTaxStatus) object;
        return !((this.ssn == null && other.ssn != null)
                || (this.ssn != null && !this.ssn.equals(other.ssn)));
    }

    @Override
    public String toString() {
        String status;
        if (federalStatus == stateStatus && stateStatus == localStatus) {
            status = "Filing status: " + federalStatus;
        } else if (federalStatus == stateStatus && stateStatus != localStatus) {
            status = "Federal/State status: " + federalStatus
                    + ", Local status: " + localStatus;
        } else if (federalStatus == localStatus && localStatus != stateStatus) {
            status = "Federal/Local status: " + federalStatus
                    + ", State status: " + stateStatus;
        } else {
            status = "Federal: " + federalStatus + ", State: " + stateStatus
                    + ", Local: " + localStatus;
        }
        return employee.getLastName() + ", " + employee.getFirstName()
                + " [ " + status + " ]";
    }

}
