/*
 * Copyright (C) 2006-2023 Sean Carrick.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3.0 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 * *****************************************************************************
 *  Project    :   database-entities
 *  Class      :   Account.java
 *  Author     :   Sean Carrick
 *  Created    :   Feb 15, 2023
 *  Modified   :   Feb 15, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Feb 15, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.ntos.db;

import com.pekinsoft.ntos.db.enums.AccountType;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The {@code Account} ...
 *
 * @author Sean Carrick (sean at pekinsoft dot com)
 * <dl><dt>Copyright</dt>
 * <dd>Copyright© 2006-2023 PekinSOFT Systems. All rights under copyright
 * reserved<br>
 * Released under the terms of the GNU Lesser General Public License version
 * 3.0.
 * </dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
@Entity
@Table(name = "ACCOUNTS", catalog = "", schema = "APP")
@NamedQueries({
    @NamedQuery(name = "Account.findAll",
            query = "SELECT a FROM Account a"),
    @NamedQuery(name = "Account.findByAccountNumber",
            query = "SELECT a FROM Account a WHERE a.accountNumber = :accountNumber"),
    @NamedQuery(name = "Account.findByAccountName",
            query = "SELECT a FROM Account a WHERE a.accountName = :accountName"),
    @NamedQuery(name = "Account.findByDescription",
            query = "SELECT a FROM Account a WHERE a.description = :description"),
    @NamedQuery(name = "Account.findByAccountType",
            query = "SELECT a FROM Account a WHERE a.accountType = :accountType")})
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ACCOUNT_NUMBER", nullable = false, length = 20)
    private String accountNumber;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_NAME", nullable = false, length = 30)
    private String accountName;
    @Column(length = 250)
    private String description;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_TYPE", nullable = false, length = 12)
    private AccountType accountType;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "defaultAccount")
    private List<Customer> customerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountNumber")
    private List<Ledger> ledgerList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountNumber")
    private List<Journal> journalList;

    public Account() {

    }

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Account(String accountNumber, String accountName, AccountType accountType) {
        this.accountNumber = accountNumber;
        this.accountName = accountName;
        this.accountType = accountType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public List<Customer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<Customer> customerList) {
        this.customerList = customerList;
    }

    public List<Ledger> getLedgerList() {
        return ledgerList;
    }

    public void setLedgerList(List<Ledger> ledgerList) {
        this.ledgerList = ledgerList;
    }

    public List<Journal> getJournalList() {
        return journalList;
    }

    public void setJournalList(List<Journal> journalList) {
        this.journalList = journalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountNumber != null ? accountNumber.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Account)) {
            return false;
        }
        Account other = (Account) object;
        return !((this.accountNumber == null && other.accountNumber != null)
                || (this.accountNumber != null
                && !this.accountNumber.equals(other.accountNumber)));
    }

    @Override
    public String toString() {
        return accountName + "[ " + accountNumber + " ]";
    }

}
